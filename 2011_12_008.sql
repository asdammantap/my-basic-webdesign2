-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 18, 2013 at 10:41 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `2011_12_008`
--

-- --------------------------------------------------------

--
-- Table structure for table `datadiri`
--

CREATE TABLE IF NOT EXISTS `datadiri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dpn` varchar(20) NOT NULL,
  `nama_blk` varchar(20) NOT NULL,
  `tmpt_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kdpost` varchar(5) NOT NULL,
  `provinsi` varchar(12) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `ket` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `datadiri`
--

INSERT INTO `datadiri` (`id`, `nama_dpn`, `nama_blk`, `tmpt_lahir`, `tgl_lahir`, `kdpost`, `provinsi`, `telp`, `ket`) VALUES
(1, 'a', 's', 'd', '0000-00-00', '21', 'xa', '123', 'asssaa'),
(2, 's', 't', 'a', '0000-00-00', '21', 'c', '213', 'asasa'),
(3, 'sas', 'asa', 'qwq', '0000-00-00', '213', 'asa', '121', 'asasa'),
(4, 'asdam', 'fungka', 'london', '2000-10-12', '21331', 'washington', '321421', 'asal'),
(5, 'asssss', 'adada', 'asada', '2000-10-11', '32112', 'banten', '654321', 'asal'),
(6, 'a', 'c', 'asa', '0000-00-00', '1221', 'asa', '123344', 'asal'),
(7, 'aaa', 'qqq', 'qwq', '0000-00-00', '111', 'a', '12', 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `kartukeluarga`
--

CREATE TABLE IF NOT EXISTS `kartukeluarga` (
  `nokartukeluarga` varchar(8) NOT NULL,
  `namakepalakeluarga` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  PRIMARY KEY (`nokartukeluarga`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kartukeluarga`
--

INSERT INTO `kartukeluarga` (`nokartukeluarga`, `namakepalakeluarga`, `alamat`, `kodepos`, `provinsi`) VALUES
('1', 'a', 's', '12', 'banten');

-- --------------------------------------------------------

--
-- Table structure for table `kartukredit`
--

CREATE TABLE IF NOT EXISTS `kartukredit` (
  `nokartu` varchar(8) NOT NULL,
  `namadepan` varchar(25) NOT NULL,
  `namabelakang` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `provinsi` varchar(20) NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `notelp` varchar(20) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`nokartu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kartukredit`
--

INSERT INTO `kartukredit` (`nokartu`, `namadepan`, `namabelakang`, `alamat`, `provinsi`, `kodepos`, `notelp`, `email`) VALUES
('01', 'asdam', 'mantap', 'cilegon', 'banten', '42161', '371213', 'asdamgaya@mantap.com');

-- --------------------------------------------------------

--
-- Table structure for table `ktp`
--

CREATE TABLE IF NOT EXISTS `ktp` (
  `nik` varchar(8) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempatlahir` varchar(25) NOT NULL,
  `tanggallahir` date NOT NULL,
  `alamat` text NOT NULL,
  `agama` varchar(20) NOT NULL,
  `status` varchar(25) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `kewarganegaraan` varchar(3) NOT NULL,
  `berlakuhingga` date NOT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ktp`
--

INSERT INTO `ktp` (`nik`, `nama`, `tempatlahir`, `tanggallahir`, `alamat`, `agama`, `status`, `pekerjaan`, `kewarganegaraan`, `berlakuhingga`) VALUES
('1', 'a', 'w', '0000-00-00', 'a', 'Islam', 'Kawin', 't', 'WNA', '0000-00-00'),
('210', 'q', 'w', '2013-10-10', 'as', 'Hindu', 'Belum Kawin', 's', 'WNI', '2015-10-10'),
('y', 'y', 'y', '0000-00-00', 'y', 'Budha', 'Kawin', 't', 'WNI', '0000-00-00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
