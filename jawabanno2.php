<html>
<head>
<title>Jawaban No 2</title>
<body>
<?php
include 'header.php';
?>
<table border="1" width="1343">
<tr>
	<td width="300"><a href="index.php"><center>Beranda</td>
	<td colspan="2" rowspan="15"><h1>Fungsi Include dan Include_once</h1>

fungsi include() akan menyertakan dan mengevaluasi seluruh program yang ada di file yang disertakan. 
Jika terdapat error pada program yang disertakan, maka error akan ditampilkan di layar. 
Dan jika file yang disertakan ternyata tidak ditemukan (mungkin karena lokasi yang salah atau memang file tidak ada), 
maka program selanjutnya (setelah include) akan tetap dijalankan walaupun ditampilkan error. dan penyertaan file yang sama beberapa kali 
mungkin akan menyebabkan error.
sedangkan include_once memastikan bahwa file yang disertakan hanya dieksekusi sekali saja, walaupun file disertakan beberapa kali.
	</td>
</tr>
<tr>
	<td><a href="jawabanno1.php"><center>Jawaban No 1</td>
</tr>
<tr>
	<td><a href="jawabanno2.php"><center>Jawaban No 2</td>
</tr>
<tr>
	<td height="30"><a href="ADMIN2/formdatapenduduk.php"><center>Data Penduduk</td>
</tr>
<tr>
	<td><a href="ADMIN2/formdatakartukeluarga.php"><center>Data Kartu Keluarga </td>
</tr>
<tr>
	<td><a href="ADMIN2/formdatakartukredit.php"><center>Data Kartu Kredit</td>
</tr>
<tr>
	<td><a href="buatdatabase.php"><center>Cara Membuat Database</td>
</tr>
<tr>
	<td><a href="ADMIN2/formdatadiri.php"><center>Data Diri</td>
</tr>
<tr>
	<td><a href="ADMIN2/formbukutamu.php"><center>Buku Tamu</td>
</tr>
<tr>
	<td><a href="ADMIN2/formpmb.php"><center>Pendaftaran Mahasiswa Baru</td>
</tr>
<tr>
	<td height="200"><center>IKLAN....</td>
</tr>
</table>
<?php
include 'footer.php';
?>
</body>
</head>
</html>