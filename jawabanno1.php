<html>
<head>
<title>Jawaban No 1</title>
<body>
<?php
include 'header.php';
?>
<table border="1" width="1343">
<tr>
	<td width="300"><a href="index.php"><center>Beranda</td>
	<td colspan="2" rowspan="15"><h1>Pengertian Tag dalam HTML</h1>

Sebagai sebuah bahasa markup, HTML membutuhkan cara untuk memberitahu web browser bagaimana suatu text ditampilkan. Apakah text itu ditulis sebagai sebuah paragraf, list, atau sebagai link? Dalam HTML, tanda ini dikenal dengan istilah tag.

Hampir semua tag dalam HTML ditulis secara berpasangan, tag pembuka dan tag penutup, dimana objek yang dikenai perintah tag berada di antara tag pembuka dan tag penutup. Objek disini dapat berupa text, gambar, maupun video. Penulisan tag berada di antara dua kurung siku.

contoh :
1. <&i&>Hanya kumpulan beberapa kalimat<&/&i&> (tanpa tanda & fungsi nya untuk buat tulisan miring)
2. <&b&>Hanya kumpulan beberapa kalimat<&/&b&> (tanpa tanda & fungsi nya untuk buat tulisan tebal)
3. <&u&>Hanya kumpulan beberapa kalimat<&/&u&> (tanpa tanda & fungsi nya untuk buat tulisan ada garis bawahnya)

sedangkan value

Value adalah nilai yang diberikan pada sebuah atribut. Value ada beberapa jenis, bisa value text seperti left, right, center, red, blue, bisa juga angka 1, 2, 3, bisa juga bentuk pixel : 100, 200, atau bentuk persen 100%, 30%, ada juga yang bentuk hexa seperti warna yang ditulis color="#000" dll.
Untuk value juga harus relevan, tidak bisa sembarangan. Contoh : align (ini untuk mengatur posisi) tidak boleh ditulis align="1" atau sebaliknya size="center".

contoh:
1. <&center&> (tanpa tanda &)
2. <&bg color="blue"&> (tanpa tanda &)
3. <&font-size="11"&> (tanpa tanda &)
	</td>
</tr>
<tr>
	<td><a href="jawabanno1.php"><center>Jawaban No 1</td>
</tr>
<tr>
	<td><a href="jawabanno2.php"><center>Jawaban No 2</td>
</tr>
<tr>
	<td height="30"><a href="ADMIN2/formdatapenduduk.php"><center>Data Penduduk</td>
</tr>
<tr>
	<td><a href="ADMIN2/formdatakartukeluarga.php"><center>Data Kartu Keluarga </td>
</tr>
<tr>
	<td><a href="ADMIN2/formdatakartukredit.php"><center>Data Kartu Kredit</td>
</tr>
<tr>
	<td><a href="buatdatabase.php"><center>Cara Membuat Database</td>
</tr>
<tr>
	<td><a href="ADMIN2/formdatadiri.php"><center>Data Diri</td>
</tr>
<tr>
	<td><a href="ADMIN2/formbukutamu.php"><center>Buku Tamu</td>
</tr>
<tr>
	<td><a href="ADMIN2/formpmb.php"><center>Pendaftaran Mahasiswa Baru</td>
</tr>
<tr>
	<td height="200"><center>IKLAN....</td>
</tr>
</table>
<?php
include 'footer.php';
?>
</body>
</head>
</html>